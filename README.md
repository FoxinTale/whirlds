You can play these in Mars Explorer with the following URLs:

- `http://lamp.gitea.moe/whirlds/Freestyle/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Foxholes/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Lava%20Racer/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Eric's%20Maze/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Achilles%20Adventure%20Island/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Alexnb_s%20Mountain%20Lake/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/App_s%20Funsplosive%20Building/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/BluetoothBoy_s%20MEGA%20house/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Dream%20World/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Flyclub_s%20Freefallz/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Flyclub_s%20Wormhole/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/FreeForge/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Grae_s%20Ice%20Bae/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Gub_s%20Dungeon/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Gub_s%20Potholes/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Gub_s%20Tubees/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/KingDvo_s%20Island/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/LittleDoug_s%20Floating%20Islands/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Seb_s%20Cliff%20Hangers/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/SkyPillars.utw`
- `http://lamp.gitea.moe/whirlds/Sunken%20Embassy/Whirld.utw`
- `http://lamp.gitea.moe/whirlds/Ultra%20WOOPland/Whirld.utw`
